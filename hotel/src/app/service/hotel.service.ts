import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class HotelService {

  constructor(private http: Http) { }

gethotels() {
      return this.http.get('http://localhost:3000/hotel/getHotels').map((data) => data.json());
}


findHotel(a) {
      const a1 = {
        name: a
      }
      return this.http.post('http://localhost:3000/hotel/findHotel', a1).map((data) => data.json());
}

findHotelbyPrice(price) {
  const a = {
    price: price
  }
  return this.http.post('http://localhost:3000/hotel/findHotelbyPrice', a).map((data) => data.json());
}

addHotel(hotel) {
      const hote = {
        name: hotel.name,
        stars: 0,
        description: hotel.description,  
        price: hotel.price,
        photo: hotel.photo,
        createdBy: hotel.createdBy,
        latitude: hotel.latitude,
        longitude: hotel.longitude,
      }
      return this.http.post('http://localhost:3000/hotel/newHotel', hote).map((data) => data.json());
    }
    getHotelById(id) {
      const objj = {
        id: id
      }
      return this.http.post('http://localhost:3000/hotel/getHotelsById', objj).map((data) => data.json());
    }

    updateHotel(hotel) {
      return this.http.post('http://localhost:3000/hotel/updateHotel', hotel).map((data) => data.json());
    }

    likePressed(likeObj) {
      return this.http.post('http://localhost:3000/hotel/likeHotel', likeObj).map((data) => data.json());
    }
    disLikePressed(likeObj) {
      return this.http.post('http://localhost:3000/hotel/dislikeHotel', likeObj).map((data) => data.json());
    }

    postComment(id, name, comment, rat) {
      const blogData = {
        id: id,
        name: name,
        comment: comment,
        stars: rat
      }
      return this.http.post('http://localhost:3000/hotel/comment', blogData).map(data => data.json());
    }
    
 }
