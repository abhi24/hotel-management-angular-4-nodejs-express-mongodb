import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Subject } from 'rxjs/Rx';

@Injectable()
export class AuthService {
  authToken= new Subject();

  constructor(private http: Http) { }

  registerUser(user) {
      return this.http.post('http://localhost:3000/authentication/register', user).map((data) => data.json());
  }

  storeKeys(peerKey, a) {
    console.log(peerKey);
    const obj = {
      name: a,
      key: peerKey
    };
    return this.http.post('http://localhost:3000/chat/chatSave', obj).map(data => data.json());
  }

  getKeys() {
    return this.http.get('http://localhost:3000/chat/chatPeople').map((data) => data.json());
  }

  isAuthenticated() {
    return this.authToken != null;
  }

  deleteKey(a: string) {
    const blog = {
      key : a
    }
    console.log(blog);
    return this.http.put('http://localhost:3000/chat/chatDelName/' + a, blog).map(data => data.json());
  }

  checkUserLogin(user) {
    var userval = {
      username: user.username,
    }
    return this.http.post('http://localhost:3000/authentication/userLogin', userval).map((data) =>
      data.json());
  }
}
