import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  loggedId;
  constructor() { }

  ngOnInit() {
    console.log('Hi' + this.loggedId);
    this.loggedId = localStorage.getItem("loggedIn");
    console.log('Hi' + this.loggedId);
  }
  onLogoutClicked() {
    window.localStorage.clear();
    window.location.reload();
  }
}
