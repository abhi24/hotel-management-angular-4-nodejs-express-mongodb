import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../service/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  addDataForm: FormGroup;
  constructor(private authSer: AuthService) { }
message;
messageClass;

  ngOnInit() {
    this.addDataForm = new FormGroup({
      'email': new FormControl(null, Validators.required),
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    console.log('hi');
    this.authSer.registerUser(this.addDataForm.value).subscribe(data => {

      if(data.success) {
          this.message = 'User Saved';
          this.messageClass = 'alert alert-success';
      } else {
        this.message = data.message;        
        this.messageClass = 'alert alert-danger';        
      }
    });

  }

}
