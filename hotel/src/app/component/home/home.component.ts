import { MapsAPILoader } from '@agm/core';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  imaging = ["http://cache.marriott.com/propertyimages/b/brubr/brubr_main03.jpg",
    "http://www.huttonhotel.com/images/main_pics/hutton_standard_king_view.jpg",
    "https://www.viajejet.com/wp-content/viajes/Piscina-exterior-del-Hotel-Urban-de-Madrid.jpg"];

  @ViewChild('search')
  public searchElementRef: ElementRef;
  imageSelected;
  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }



  ngOnInit() {

    this.imageSelected = "https://www.viajejet.com/wp-content/viajes/Piscina-exterior-del-Hotel-Urban-de-Madrid.jpg";
    console.log(this.imaging[Math.floor(Math.random() * this.imaging.length)]);
    setInterval(() => {
      this.imageSelected = this.imaging[Math.floor(Math.random() * this.imaging.length)];
    }, 2000);

    this.searchControl = new FormControl();
    this.zoom = 14;
    this.latitude = 28.6229;
    this.longitude = 77.089;
    this.searchControl = new FormControl();

    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }


}
