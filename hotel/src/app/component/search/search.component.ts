import { Component, OnInit, Output, NgZone, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { HotelService } from '../../service/hotel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [`
  agm-map {
     height: 300px;
   }
`],
})
export class SearchComponent implements OnInit {
  @ViewChild('search')
  public searchElementRef: ElementRef;

  hotelsDetails;
  filteredStatus;
  hotelNames;
  hote;
  searchVal;
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  location;
  hotelArray = [];
  priceOrName;
  priceMinFilter: number;
  priceFilterForm: FormGroup;
  @Output() filterPrice: EventEmitter<{ priceMin: number, }> = new EventEmitter<{ priceMin: number, }>();

  _priceOptions = [
    { "valueP": null },
    { "valueP": 50 },
    { "valueP": 100 },
    { "valueP": 200 },
    { "valueP": 300 },
    { "valueP": 400 }
  ]

  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private hotelSer: HotelService) { }

  ngOnInit() {

    // location data

    this.zoom = 12;
    this.latitude = 0;
    this.longitude = 0;
    this.searchControl = new FormControl();

    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }

    // location data


    this.priceFilterForm = new FormGroup({
      priceMin: new FormControl('any')
    });
    this.priceFilterForm.controls['priceMin'].valueChanges.subscribe(
      (data: any) =>
        this.priceOrName = data
    )
  }

  locationFixed() {
    console.log(this.latitude + "    " + this.longitude);
    for (const l1 of this.hotelNames.message) {
    //  console.log('He');
      console.log(l1.latitude);
      if (this.latitude - l1.latitude < 0.024 && this.longitude - l1.longitude < 0.024 &&
        this.latitude - l1.latitude > -0.024 && this.longitude - l1.longitude > -0.024 && (this.location === '1-3 km')) {
          console.log('He');
        this.hote.push(l1);
      } else if (this.latitude - l1.lat < 0.040 && this.longitude - l1.longitude < 0.040 &&
        this.latitude - l1.latitude > -0.040 && this.longitude - l1.longitude > -0.040 && (this.location === '3-6 km')) {
        this.hote.push(l1);
      } else if (this.latitude - l1.latitude < 0.060 && this.longitude - l1.longitude < 0.060
        && this.latitude - l1.latitude > -0.060 && this.longitude - l1.longitude > -0.060 && (this.location === '6-10 km'))  {
        console.log('He');
        console.log(l1)
        this.hotelArray.push(l1);
     //   this.hote = l1;
      } else {

      }
    }
    this.hote = this.hotelArray;
    console.log("Value is" + this.hotelArray);
  }

  firstDropDownChanged(a) {
    this.location = a;
    console.log(this.location);
    this.hotelSer.gethotels().subscribe((data) => {
      console.log(data);
      this.hotelNames = data;

    });
  }
  
  priceDropDownChanged(a) {
    this.priceOrName = a;
  }

  onSubmit(a) {
    this.hotelSer.findHotel(a).subscribe((data) => {
      console.log(data);
      if (data.message.length) {
        this.hote = data.message;
      }
    });
  }

  searchByPrice() {
    this.hotelSer.findHotelbyPrice(this.priceOrName).subscribe((data) => {
      console.log(data);
      this.hote = data.message;
    });
  }

  searching(a) {
    console.log(a);
    this.searchVal = a;
  }
}
