import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../service/auth.service';



@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  addSignInData: FormGroup;
  message;
  messageClass;
  userValue;
  constructor(private authSer: AuthService, private router: Router,  private route: ActivatedRoute) { }

  ngOnInit() {
    this.addSignInData = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    console.log(this.addSignInData.value);
    this.authSer.checkUserLogin(this.addSignInData.value).subscribe(data => {
      console.log(data);
      if(data.success) {
         this.userValue = data.message.username;
         window.localStorage.setItem('ObjectIdIs', data.message._id);
         window.localStorage.setItem('username', this.userValue);
        this.message = 'User Found';
        this.messageClass = 'alert alert-success';
        localStorage.setItem("loggedIn", "true");
        this.router.navigate(['home']);
    } else {
      this.message = 'User not registered';
      this.messageClass = 'alert alert-danger';

    }
    });
  }
}
