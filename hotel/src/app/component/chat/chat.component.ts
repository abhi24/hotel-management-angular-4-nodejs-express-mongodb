import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../service/auth.service';



@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @ViewChild('myvideo') myVideo: any;
  title = 'Welcome Abhishek';
  peer;
  anotherid: string;
  mypeerid;
  peopleOnline= [];
  callActive= false;
  loggedInUser = '';
  localstream;
  constructor(private authSer: AuthService) {
  }

  ngOnInit() {
    this.authSer.getKeys().subscribe((data) => {
      console.log(data);
      this.peopleOnline = data.message;
      console.log('data stored wow');
    });

    const video = this.myVideo.nativeElement;
    this.peer = new Peer({key: 'es9qrwvr5xe9izfr'});
    setTimeout(() => {
      this.mypeerid = this.peer.id;
      window.localStorage.setItem('peerKey', this.mypeerid);
      const a = window.localStorage.getItem('nameIs');
      this.authSer.storeKeys(this.mypeerid, 'Kayla').subscribe((data) => {
        console.log('data stored by Kay');
      });
    }, 3000);

    this.peer.on('connection', function(conn) {
      conn.on('data', function(data){
        console.log(data);
      });
    });
    const n = <any>navigator;
    n.getUserMedia = ( n.getUserMedia || n.webkitGetUserMedia || n.mozGetUserMedia  || n.msGetUserMedia );
    this.peer.on('call', function(call) {
      n.getUserMedia({video: true, audio: true}, function(stream) {
        call.answer(stream);
        call.on('stream', function(remotestream){
          video.src = URL.createObjectURL(remotestream);
          video.play();
        });
      }, function(err) {
        console.log('Failed to get stream', err);
      });
    });
  }

  connect() {
    const conn = this.peer.connect(this.anotherid);
    conn.on('open', function(){
      conn.send('Message from that id');
    });
  }

  onVideoCall(pe: string, val: boolean) {

    console.log('value of is : ', pe, val);
    const video = this.myVideo.nativeElement;
    const localvar = this.peer;
    const fname = pe;
    const n = <any>navigator;
    n.getUserMedia = ( n.getUserMedia || n.webkitGetUserMedia || n.mozGetUserMedia || n.msGetUserMedia );

    if (val) {

      this.callActive = true;
      n.getUserMedia({video: true, audio: true}, function (stream) {
        const call = localvar.call(fname, stream);
        call.on('stream', function (remotestream) {
          this.localstream = stream;
          video.src = URL.createObjectURL(remotestream);
          video.play();
        });
      }, function (err) {
        console.log('Failed to get stream', err);
      });
    } else {
      video.pause();
      video.src = '';
      this.authSer.getKeys().subscribe((data) => {
        this.loggedInUser = data;
        console.log(this.loggedInUser);
      });

        const a = window.localStorage.getItem('username');
        // delete by Object Id
      this.authSer.deleteKey(a).subscribe((data) => {
        console.log(data);
      });
    }
  }
}
