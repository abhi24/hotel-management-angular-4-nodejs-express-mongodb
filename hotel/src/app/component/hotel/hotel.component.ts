import { HotelService } from '../../service/hotel.service';
import { AuthService } from '../../service/auth.service';

import { Router, ActivatedRoute } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import { Component, ElementRef, NgZone, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Response } from '@angular/http';



@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
  agm-map {
     height: 300px;
   }
`],
})

export class HotelComponent implements OnInit {


  countries = [1, 2, 3, 4, 5];
  hotels;
  idTobeEdited;
  userLoggedIn;
  hotelToBeEdited;
  hotelForm: FormGroup;
  hotelAddorEdit = false;
  message;
  messageClass;
  EditMode = false;
  newComment = [];
  enabledComments = [];
  commentId;
  commentButtonPressed = false;
  commentFormValue: FormGroup;
  constructor(private hotelSer: HotelService,
    private route: Router, private routerr: ActivatedRoute,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private authSer: AuthService) {

  }

  ngOnInit() {

    this.authSer.authToken.subscribe((data: string) => {
        console.log("Subject is "  + data);
    });
    
    this.hotelAddorEdit = true;
    this.userLoggedIn = window.localStorage.getItem('username');

    this.commentForm();

    this.hotelForm = new FormGroup({
      'name': new FormControl(null),
      'price': new FormControl(null),
      'description': new FormControl(null),
      'photo': new FormControl(null),
      'createdBy': new FormControl(this.userLoggedIn),
    });
    this.hotelSer.gethotels().subscribe(data => {
      this.hotels = data.message;
    })
  }


  onEditClicked(id) {
    this.idTobeEdited = id;
    this.EditMode = true;

    console.log(id);
    this.hotelSer.getHotelById(id).subscribe(data => {
      this.hotelToBeEdited = data;
      console.log(this.hotelToBeEdited);
      this.hotelForm = new FormGroup({
        'name': new FormControl(this.hotelToBeEdited.message.name),
        'price': new FormControl(this.hotelToBeEdited.message.price),
        'description': new FormControl(this.hotelToBeEdited.message.description),
        'photo': new FormControl(this.hotelToBeEdited.message.photo),
        'createdBy': new FormControl(this.userLoggedIn),
      });
    });
  }

  newhotel() {
    this.route.navigate(['newHotel'], { relativeTo: this.routerr });
    this.hotelAddorEdit = false;
  }


  onSubmit() {
    console.log(this.hotelForm.value);

    if (this.EditMode) {
      console.log(this.hotelForm.value);
      const hoteObj = {
        _id: this.idTobeEdited,
        name: this.hotelForm.value.name,
        price: this.hotelForm.value.price,
        description: this.hotelForm.value.description,
        photo: this.hotelForm.value.photo,
        createdBy: this.hotelForm.value.createdBy,
      }
      console.log(hoteObj);

      this.hotelSer.updateHotel(hoteObj).subscribe(data => {
        console.log(data);
        if (data.success) {
          this.message = 'Hotel Saved';
          this.messageClass = 'alert alert-success';

        } else {
          this.message = 'Error In saving Hotel';
          this.messageClass = 'alert alert-danger';
        }
      });

    } else {
      this.hotelSer.addHotel(this.hotelForm.value).subscribe(data => {
        console.log(data);
        if (data.success) {
          this.message = 'Hotel Saved';
          this.messageClass = 'alert alert-success';

        } else {
          this.message = 'Error In saving Hotel';
          this.messageClass = 'alert alert-danger';
        }
      });
    }
  }

  onLikePressed(id) {
    var likeObj = {
      id: id,
      likedBy: this.userLoggedIn
    }
    this.hotelSer.likePressed(likeObj).subscribe(data => {
      console.log(data);
    });
  }

  postComment(id) {
    this.commentId = id;
    this.message = 'Comment made Successfully';
    this.messageClass = 'alert alert-success';
  }

  cancelComment(id) {
    const index = this.newComment.indexOf(id);
    this.newComment.splice(index, 1);
    this.commentFormValue.reset();
  }

  commentForm() {
    this.commentFormValue = new FormGroup({
      'comment': new FormControl(null),
      'cou': new FormControl(null),
    });

  }


  draftComment(id) {
    this.newComment = [];
    this.newComment.push(id);
  }

  onCommentFormSubmit() {
    var ab = this.commentFormValue.get('comment').value;
    var rat = this.commentFormValue.get('cou').value;
    console.log(ab, rat);
    this.hotelSer.postComment(this.commentId, this.userLoggedIn, ab, rat).subscribe((data) => {
      console.log(data);
    });
    setInterval(() => {
      window.location.reload();
    }, 2000);

  }

  expand(id) {
    this.enabledComments.push(id);
  }

  collapse(id) {
    const index = this.enabledComments.indexOf(id);
    this.enabledComments.splice(index, 1);
  }

  ondisLikePressed(id) {
    var likeObj = {
      id: id,
      name: this.userLoggedIn
    }
    this.hotelSer.disLikePressed(likeObj).subscribe(data => {
      console.log(data);
    });
  }
}
