import { HotelService } from '../../../service/hotel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Response} from '@angular/http';


@Component({
  selector: 'app-new-hotel',
  templateUrl: './new-hotel.component.html',
  styles: [`
  agm-map {
     height: 300px;
   }
`],
})
export class NewHotelComponent implements OnInit {
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  message;
  messageClass;
  public zoom: number;
  hotelForm: FormGroup;
  userLoggedIn;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  
  constructor( private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private hotelSer: HotelService) { }

  ngOnInit() {

    this.userLoggedIn = window.localStorage.getItem('username');
    this.hotelForm = new FormGroup({
        'name': new FormControl(null),
        'price': new FormControl(null),
        'description': new FormControl(null),
        'photo': new FormControl(null),
        'createdBy': new FormControl(this.userLoggedIn),
    });
    
    this.zoom = 14;
    this.latitude = 28.6229;
    this.longitude = 77.089;
    this.searchControl = new FormControl();
  
    this.setCurrentPosition();
  
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
  
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
  
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  onSubmit() {
    const HObj= {
      name: this.hotelForm.value.name,
      price: this.hotelForm.value.price,
      description: this.hotelForm.value.description,
      photo: this.hotelForm.value.photo,
      createdBy: (this.userLoggedIn),
      latitude: this.latitude,
      longitude: this.longitude
    }
    console.log(HObj);
    this.hotelSer.addHotel(HObj).subscribe(data => {
      console.log(data);
      if(data.success) {        
        this.message = 'Hotel Saved';
        this.messageClass = 'alert alert-success';   
        
    } else {
      this.message = 'Error In saving Hotel';        
      this.messageClass = 'alert alert-danger';      
    }
    });
  }
}

