import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import {HomeComponent} from './component/home/home.component';
import {RegisterComponent} from './component/register/register.component';
import {SigninComponent} from './component/signin/signin.component';
import {HotelComponent} from './component/hotel/hotel.component';
import {NewHotelComponent} from './component/hotel/new-hotel/new-hotel.component';
import {SearchComponent} from './component/search/search.component';
import {AuthGuard} from './guard/auth.guard';

import {MapsComponent} from './component/maps/maps.component';
import {ChatComponent} from './component/chat/chat.component';

const appRoutes: Routes = [
    {path: 'home'  , component: HomeComponent },
    {path: 'register'  , component: RegisterComponent },
    {path: 'signin'  , component: SigninComponent },
    {path: 'hotel'  , component: HotelComponent , canActivate : [AuthGuard] , children: [
        {path: 'newHotel'  , component: NewHotelComponent },
    ]},
    {path: 'search'  , component: SearchComponent, canActivate : [AuthGuard] },
    {path: 'maps'  , component: MapsComponent },
    {path: 'chat'  , component: ChatComponent },

];

@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(appRoutes)],
    providers: [],
    bootstrap: [],
    exports: [RouterModule]
  })

export class AppRoutingModule { }
