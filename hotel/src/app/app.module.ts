import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './component/navbar/navbar.component';
import { HomeComponent } from './component/home/home.component';
import { SigninComponent } from './component/signin/signin.component';
import { RegisterComponent } from './component/register/register.component';
import { AuthService } from './service/auth.service';
import { HotelService } from './service/hotel.service';
import { HttpModule } from '@angular/http';
import { HotelComponent } from './component/hotel/hotel.component';
import { FilterPipe } from './component/search/filter.pipe';
import { SearchComponent } from './component/search/search.component';
import { SearchresComponent } from './component/search/searchres/searchres.component';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';
import { MapsComponent } from './component/maps/maps.component';
import { AuthGuard } from './guard/auth.guard';
import { NewHotelComponent } from './component/hotel/new-hotel/new-hotel.component';
import { ChatComponent } from './component/chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    SigninComponent,
    RegisterComponent,
    HotelComponent,
    SearchComponent,
    FilterPipe,
    SearchresComponent,
    MapsComponent,
    NewHotelComponent,
    ChatComponent
  ],

  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCOn4RZ7yNHg0xysGXZN2tDVRnw-DjrldE',
      libraries: ['places']
    }),
  ],
  providers: [AuthService, HotelService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
