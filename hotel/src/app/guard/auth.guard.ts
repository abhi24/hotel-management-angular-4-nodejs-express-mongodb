import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '../service/auth.service';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AuthGuard {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate() {
      var a : boolean = localStorage.getItem("loggedIn") == "true"; 
      console.log(a);
      if(a) {
        return a;
      } else {
        this.router.navigate(['/signin']);
      }
  }
}