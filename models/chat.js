var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var chat = new Schema({
    name: {type: String, required: true},
    key: {type: String, required: true},
});

module.exports = mongoose.model('chat', chat);