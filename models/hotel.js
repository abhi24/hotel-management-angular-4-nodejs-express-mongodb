var mongoose = require('mongoose');
var Schema  = mongoose.Schema;

var newHotel = new Schema({
    name: { type: String,required: true},
    stars: { type: Number,min: 0,max: 5,default: 0},
    description: {type: String,required: true},
    price: {type: Number, required: true},
    photo: {type: String,required: true},
    createdBy: {type: String, required: true},
    createdAt: {type: String},
    //reviews
    likes: {type: Number, default: 0},
    likedBy: {type: Array},
    dislikes: {type: Number, default: 0},
    dislikedBy: {type: Array},
    comments: [{
        comment: {type:String},
        commentator: {type: String}
    }],
    latitude : {type: Number, required: true},
    longitude : {type: Number, required: true},
});
    

module.exports = mongoose.model('Hotel', newHotel);