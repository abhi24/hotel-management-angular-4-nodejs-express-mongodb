var mongoose = require('mongoose');
var Schema  = mongoose.Schema;


var emailLength = (email) => {
    if(!email || email.length <5){
        return false;
    } else {
        return true;
    }
};

const emailValidator = [{
        validator: emailLength, message: 'Email is invalid'}
];


var usernameLength = (username) => {
    if(!username || username.length <5){
        return false;
    } else {
        return true;
    }
};

const usernameValidator = [{
    validator: usernameLength, message: 'Username is invalid'}
];


var userSchema = new Schema({
   email: { type: String, required: true, unique: true, lowercase: true},
   username: { type: String, required: true, unique: true, lowercase: true},
   password: { type: String, required: true}
});

module.exports = mongoose.model('User', userSchema);
