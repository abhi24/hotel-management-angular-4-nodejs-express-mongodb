var express = require('express');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var cors = require('cors');
//var blogs = require('./routes/blogs');
var authentication = require('./routes/authentication');
var hotel = require('./routes/hotel');
var chat = require('./routes/chat');

var bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://test:test@ds135514.mlab.com:35514/hotel_abhi', function(){
    console.log('Connected to Db');
});

app.use(cors({
    origin: 'http://localhost:4200'
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(__dirname + '/client/dist/'));

app.use('/authentication', authentication);
app.use('/hotel', hotel);
app.use('/chat', chat);


app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/client/dist/index.html'));
});


app.listen(3000, () => {
    console.log('Server Running');
});