# README #

This app is developed using **Angular 4, NodeJS, MongoDb, Express, Bootstrap**

## What is this repository for? ###
This repository is a Hotel management system where users and admin can:
> ### User
> 
> 1.   Create an account and login
> 2.   Book Hotels
> 3.   Rate and comment on hotels
> 4.   Search for hotels on Map based on current location
>
> ### Admin
>
> 1.   Add/Delete Hotels
> 2.   Book Hotels


### Running the Project

We're using Node.js, NPM (Node Package Manager). To preview code locally, you'll need to install Node and NPM, then run the following commands from a terminal window, in the repo directory:

> $ npm install
>
> $ ng serve --port 4040

Those commands do the following:

npm install will install the necessary node.js packages to develop on this project
Once run, you can preview the site by pointing a web server at the dist directory (see below).

ng serve will run the code on local host server 4040


## Images
>
   **Main Screen**
>
![Alt text](https://bytebucket.org/abhi24/hotel-management-angular-4-nodejs-express-mongodb/raw/4ef3562ba0d969cd1f195d6495419e455d0ca494/hotel/Screen%20Shot%202018-04-02%20at%207.46.21%20PM.png?token=7ff7c7f02366c2819f4138cd35a26612be6465ca)
>
  **List of hotels where user can comment like dislike**
>
![Alt text](https://bytebucket.org/abhi24/hotel-management-angular-4-nodejs-express-mongodb/raw/2ff2b0100c2b3a6b30506957d1f0adbb8ab18614/hotel/Hots.png?token=f8076fd12c11706720a74d12eb2e74d9416ab159)

>
  **Search Based on Current Location in Google Maps**
>
![Alt text](https://bytebucket.org/abhi24/hotel-management-angular-4-nodejs-express-mongodb/raw/4ef3562ba0d969cd1f195d6495419e455d0ca494/hotel/Screen%20Shot%202018-04-02%20at%207.48.56%20PM.png?token=9b83148a4e371e2b1c555a175c2da6945a653eeb)

>
   **Login** 
>
![Alt text](https://bytebucket.org/abhi24/hotel-management-angular-4-nodejs-express-mongodb/raw/4ef3562ba0d969cd1f195d6495419e455d0ca494/hotel/Screen%20Shot%202018-04-02%20at%207.49.20%20PM.png?token=5a976a2d7ed77f22c32d7b95ba4088246e4797ca) 

>
   **Book Hotel**
>
![Alt text](https://bitbucket.org/abhi24/hotel-management-angular-4-nodejs-express-mongodb/src/4ef3562ba0d969cd1f195d6495419e455d0ca494/hotel/Screen%20Shot%202018-04-02%20at%207.47.33%20PM.png?at=master&fileviewer=file-view-default) 


## Who do I talk to? ###

>Abhishek Gupta