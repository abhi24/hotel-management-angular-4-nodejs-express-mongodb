var User = require('../models/user');
var express = require('express');
var mongojs = require('mongojs');
var router = express.Router();

var db = mongojs('mongodb://test:test@ds135514.mlab.com:35514/hotel_abhi', ['users']);

router.post('/userLogin', (req, res) => {
    if(!req.body.username) {
        res.json({success: false, message: 'Provide an username'});
    } else {      
        db.collection('users').findOne({username: req.body.username}, (err, user) => {
            if(!user) {
                res.json({success: false, message: user});
            } else {
                res.json({success: true, message: user});
            }
        });
    }
});

router.post('/register', (req, res) => {
    if(!req.body.email) {
        res.json({success: false, message: 'Provide an email'});
    }  else {
        if(!req.body.username) {
            res.json({success: false, message: 'Provide an username'});
        } else {
            if(!req.body.password) {
                res.json({success: false, message: 'Provide an password'});
            } else {
                    var newUser = new User({
                        email: req.body.email,
                        username: req.body.username,
                        password: req.body.password,
                    });
                    newUser.save((err) => {
                        if(err) {
                            if(err.code === 11000) {
                                res.json({success: false, message: 'Duplicate Username'});                                
                            } else {
                                res.json({success: false, message: 'Could not save user', err});                                
                            }
                        } else {
                            res.json({success: true, message: 'User Saved'});
                        }
                    });
            }
        }
    }
});


module.exports = router;