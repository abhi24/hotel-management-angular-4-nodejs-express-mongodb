var Hotel = require('../models/hotel');
var express = require('express');
var mongojs = require('mongojs');
var router = express.Router();
var db = mongojs('mongodb://test:test@ds135514.mlab.com:35514/hotel_abhi', ['hotels']);

router.post('/newHotel', (req, res) => {
    if(!req.body.name) {
        res.json({success: false, message: 'Provide a hotel name'});      
    } else {
        if(!req.body.description) {
            res.json({success: false, message: 'Provide a hotel description'});      
        } else {
            if(!req.body.description) {
                res.json({success: false, message: 'Provide a hotel description'});      
            } else {
                if(!req.body.photo) {
                    res.json({success: false, message: 'Provide a hotel image'});      
                } else {
                    if(!req.body.createdBy) {
                        res.json({success: false, message: 'Provide a createdBy'});      
                    } else {
                        if(!req.body.price) {
                            res.json({success: false, message: 'Provide a price'});      
                        } else {         
                            if(!req.body.latitude) {
                                res.json({success: false, message: 'Provide a latitude'});      
                            } else { 
                                if(!req.body.longitude) {
                                    res.json({success: false, message: 'Provide a longitude'});      
                                } else { 
                            
                    var newHotel = new Hotel({
                        name: req.body.name,
                        description: req.body.description,
                        photo: req.body.photo,
                        createdBy: req.body.createdBy,
                        price:req.body.price,
                        stars: req.body.stars,
                        longitude: req.body.longitude,
                        latitude: req.body.latitude,
                    });

                    newHotel.save((err) => {
                        if(err) {
                            if(err.code === 11000) {
                                res.json({success: false, message: 'Duplicate Hotel'});                                
                            } else {
                                res.json({success: false, message: 'Could not save Hotel', err});                                
                            }
                        } else {
                            res.json({success: true, message: 'Hotel Saved'});
                        }
                    });

                                }            
                            }
                        }
                    }       
                }
            }
        }
    }
})

router.get('/getHotels', (req, res) => {
    db.hotels.find({}, (err, data) => {
        if(err){
            res.json({success: false, message: err}); 
        }else {
            res.json({success: true, message: data}); 
        }
    });
});

router.post('/getHotelsById', (req , res) => {
       if(!req.body.id){
        res.json({success: false, message: 'No id present'});
       } else {
           
        db.collection('hotels').findOne({_id: mongojs.ObjectId(req.body.id)}, (err, data) => {
            if(err) {
                res.json({success: false, message: 'error'});
            } else {
                res.json({success: true, message: data});
            }
        });  
       } 
});

router.post('/updateHotel', (req , res) => {       
    db.collection('hotels').update({_id: mongojs.ObjectId(req.body._id)}, { $set: { 'name': req.body.name , 'description': req.body.description,'price': req.body.price} }, function(err, data)  {
       if (err) {
            throw err;   
        }
        else {
            res.json({success: true, message: 'Updated'});                   
        }
     });
});

router.post('/likeHotel', (req , res) => {       
   
       if (!req.body.id) {
             res.json({success: false, message: 'Id not present'});  
        }
        else {
            db.collection('hotels').findOne({_id: mongojs.ObjectId(req.body.id)}, (err, data) => {
                if(err) {
                    res.json({success: false, message: 'error'});
                } else {
                    if(data.dislikedBy.includes(req.body.likedBy)) {
                        var a = data.likes;
                        a++;
                        var b = data.dislikes;
                        b--;

                        db.collection('hotels').update({_id: mongojs.ObjectId(req.body.id)}, 
                        { $push: { 'likedBy': req.body.likedBy},
                        $set: {'likes': a, 'dislikes': b}, 
                        $pull: { 'dislikedBy': req.body.likedBy} },  
                        function(err, data)  {
                            if (err) {
                                throw err;   
                            } else {
                                res.json({success: true, message: 'Data Liked Successfully'});
                            }
                        }
                    )
                    } else {
                        if(data.likedBy.includes(req.body.likedBy)) {
                            res.json({success: false, message: 'You have already Liked it'});
                        } else {
                            var b = data.likes;
                            b++;
                            db.collection('hotels').update({_id: mongojs.ObjectId(req.body.id)}, 
                            { 
                            $set: {'likes': b}, 
                            $push: { 'likedBy': req.body.likedBy} },
                            function(err, data)  {
                                if (err) {
                                    throw err;   
                                } else {
                                    res.json({success: true, message: 'Data Liked Successfully'});
                                }
                            }
                        )
                        }
                      
                    }
                }
            });             
        }
});

router.post('/dislikeHotel', (req , res) => {       
    
        if (!req.body.id) {
              res.json({success: false, message: 'Id not present'});  
         }
         else {
             db.collection('hotels').findOne({_id: mongojs.ObjectId(req.body.id)}, (err, data) => {
                 if(err) {
                     res.json({success: false, message: 'error'});
                 } else {
                     if(data.likedBy.includes(req.body.name)) {
                         var a = data.likes;
                         a--;
                         var b = data.dislikes;
                         b++;
 
                         db.collection('hotels').update({_id: mongojs.ObjectId(req.body.id)}, 
                         { $pull: { 'likedBy': req.body.name},
                         $set: {'likes': a, 'dislikes': b}, 
                         $push: { 'dislikedBy': req.body.name}
                         },  
                         function(err, data)  {
                             if (err) {
                                 throw err;   
                             } else {
                                 res.json({success: true, message: 'Data DisLiked Successfully'});
                             }
                         }
                     )
                     } else {
                             var b = data.dislikes;
                             b++;
                             db.collection('hotels').update({_id: mongojs.ObjectId(req.body.id)}, 
                             { 
                             $set: {'dislikes': b}, 
                             $push: { 'dislikedBy': req.body.dislikedBy} },
                             function(err, data)  {
                                 if (err) {
                                     throw err;   
                                 } else {
                                     res.json({success: true, message: 'Data Liked Successfully'});
                                 }
                             }
                         )
                     }
                 }
             });             
         }
});

 router.post('/comment', (req, res) => {
    if (!req.body.comment) {
        res.json({success: false, message: 'No comment provided'});
    } else {
        if(!req.body.name) {
            res.json({success: false, message: 'No name provided'});
        } else {
            if(!req.body.id) {
                res.json({success: false, message: 'No id provided'});
            } else {   
                    const obj = {
                        comment: req.body.comment,
                        commentator: req.body.name
                    }


                    db.collection('hotels').update({_id: mongojs.ObjectId(req.body.id)},
                    { $push: { 'comments': obj}},
                     function(err, data)  {
                       if (err) {
                           throw err;   
                       } else {
                        db.collection('hotels').findOne({_id: mongojs.ObjectId(req.body.id)}, (err, data) => {
                            if(err) {
                                res.json({success: false, message: 'error'});
                            } else {
                                var star_total = (Number(data.stars) + Number(req.body.stars));
                                var starLength = data.comments.length;
                                
                                var finalValue = star_total/starLength;  
                                
                                db.collection('hotels').update({_id: mongojs.ObjectId(req.body.id)},
                                   { $set: { 'stars': finalValue}},
                                    function(err, data)  {
                                        if (err) {
                                            throw err;   
                                        } else {
                                            res.json({success: true, message: data});
                                        }});
                                }
                        });                         
                       }
                   });


                }
            }
        }
});
    
router.post('/findHotel', (req, res) => {
        if(!req.body.name) {
            res.json({success: false, message: 'provide a name'});
        } else {
            var query = { name: req.body.name};
            var q1 = {"name" : {$regex : req.body.name}};
            db.collection('hotels').find(q1, (err, data) => {
                if(data.message) {
                    res.json({success: false, message: 'error'});
                } else {
                    res.json({success: true, message: data});
                }
            });  
        }
});

router.post('/findHotelbyPrice', (req, res) => {
    if(!req.body.price) {
        res.json({success: false, message: 'provide a price'});
    } else {
        var priceIs = { "price" : { $gt: parseInt(req.body.price)}};     
        db.collection('hotels').find(priceIs, (err, data) => {
            if(data.message) {
                res.json({success: false, message: 'error'});
            } else {
                res.json({success: true, message: data});
            }
        });  
    }
})
    
module.exports = router;
