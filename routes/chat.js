var Chat = require('../models/chat');
var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://test:test@ds135514.mlab.com:35514/hotel_abhi', ['chats']);

router.post('/chatSave', (req, res) => {
    if (!req.body.name) {
    res.json({success: false , message: 'no name present'});
}else {
    if(!req.body.key) {
        res.json({success: false , message: 'no key present'});
    }  else {
        const prod = new Chat({
            name: req.body.name,
            key: req.body.key,
        });

        prod.save(prod, function(err , data) {
            if(err){
                res.json({success: false, message: err});
            } else {
                res.json({success: true, message: 'details saved'});
            }
        });
    }
}
});

router.get('/chatPeople', (req, res)  => {
    db.chats.find({}, (err, data) => {
        if(err) {
            res.json({success: false, message: err});
        }else {
            res.json({success: true, message: data});
        }
    })
});



router.post('/chatDelName', (req, res) => {
    if (!req.body.key) {
    res.send('No key present');
} else {
    db.collection('chats').remove({_id: mongojs.ObjectId(req.body.key)}, (err, data) => {
        if(err) {
            res.json({success: false, message: 'No id found'});
        } else {
            res.json({success: true, message: 'Deleted'});
}
});
}
});


module.exports = router;